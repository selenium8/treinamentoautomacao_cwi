package pageObjetcs;

import org.openqa.selenium.support.PageFactory;
import utils.Browser;

public class CategoryPage extends CategoryPageElementMapper {

    public CategoryPage(){
        PageFactory.initElements(Browser.getCurrentDriver(), this);}
        public boolean isPageTShirts(){return getAuthenticationPageTShirts().contains("T-SHIRTS");}

        public String getAuthenticationPageTShirts() {return nameCategoryTShirts.getText();}

        public void clickProdutoAddToProductPage(){
        BasePage.mouseOver(product);
        buttonMoreAddToProductPage.click();
        }
        public String getProductNameCategory(){return getProductNameCategory().getText();}

}
