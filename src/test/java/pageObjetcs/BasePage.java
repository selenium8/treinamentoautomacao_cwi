package pageObjetcs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.Browser;

public class BasePage extends Browser {

    public static void mouseOver(WebElement element) {
        Actions action = new Actions(Browser.getCurrentDriver());
        action.moveToElement(element).build().perform();
    }

    public static boolean waitElements(String type, String locator){

        switch (type){
            case "id":
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
                return true;
            case "cssSelector":
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));
                return true;
            case "classname":
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
                return true;
            case "xpath":
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
                return true;
            case "linkText":
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(locator)));
                return true;
        }
                return false;
    }
}
