package tests;

import com.sun.org.glassfish.gmbal.Description;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import pageObjetcs.CategoryPage;
import pageObjetcs.HomePage;
import pageObjetcs.SearchPage;
import utils.Browser;
import utils.Utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Feature("Fluxos de Teste")
public class SetupTest extends BaseTests {

    @Test
    @Description("Teste de Abrir o Browser")
    public void testOpeningBrowserAndLoadingPage(){
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl()));
        System.out.println("Abrimos o navegador");
    }

    @Test
    @Description("Teste que realiza login")
    public void testLogin(){
        //iniciar páginas
        HomePage home = new HomePage();
        LoginPage login = new LoginPage();
        MyAccountPage myAccount = new MyAccountPage();

        //clicar botao login Home
        home.clickBntLogin();
        System.out.println("Clicamos botao login");

        //ver se foi pra pagina login
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains("my-account"));
        assertTrue(login.isPageLogin());
        System.out.println("Estamos página login");

        //Realizar login
        login.doLogin();
        System.out.println("Realizamos login");

        //verificar se foi pra minha conta
        assertTrue(MyAccount.isPageMyAccount());
        System.out.println("Minha conta");


        Browser.getCurrentDriver().findElement(By.className("login")).click();
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl().concat("index.php?controller=authentication&back=my-account")));
        Browser.getCurrentDriver().findElement(By.id("email")).sendKeys("janainaslima@yahoo.com.br");
        Browser.getCurrentDriver().findElement(By.id("passwd")).sendKeys("12345");
        Browser.getCurrentDriver().findElement(By.id("SubmitLogin")).click();
        assertTrue(Browser.getCurrentDriver().getCurrentUrl().contains(Utils.getBaseUrl().concat("index.php?controller=my-account")));
        assertTrue(Browser.getCurrentDriver().findElement(By.className("page-heading")).getText().contains("MY ACCOUNT"));


    }

    @Test

    public void testSearch(){

        String quest = "dress";
        String questResultQtd = "7";

        HomePage home = new HomePage();
        SearchPage search = new SearchPage();

        home.doSearch(quest);

        assertTrue(search.isSearchPage());
        assertEquals(search.getTextLighter(), "dress");
        assertEquals(search.getTextHeading_counter(), CoreMatchers.containsString(questResultQtd));

    }


    @Test
    public void testAcessCategoryTShirts(){
        /*
        Browser.getCurrentDriver().findElement(By.id("search_query_top")).sendKeys("dress");
        Browser.getCurrentDriver().findElement(By.name("submit_search")).click();
        Browser.getCurrentDriver().findElement(By.linkText("DRESSES"));
    }
      */
        //iniciar página
        HomePage home = new HomePage();
        CategoryPage category = new CategoryPage();

        //clicar na categoria
        home.clickCategoryTShirts();

}
